This is my implementation of task A - "A multi-threaded (e.g. file-based) web server with thread-pooling implemented in Java"



For demonstration purpose some very simple content is included. 

To setup and run:

 git clone https://gitlab.com/kmersman/simplewebserver.git
 
 cd simplewebserver

 <unzip html.zip to some writeable directory, e.g. ../html>

 java -jar .\target\simplewebserver-0.0.1-SNAPSHOT.jar -p 8082 -k 3 -t 4 -d ../html



Just type java -jar .\target\simplewebserver-0.0.1-SNAPSHOT.jar  to see options. All Options can be defaulted, except -d to provide the directory to serve content from.


Overall I focused on implementing the Core functionality myself as I understood this to be the task at hand. I did not uses too many external libraries or existing tool, like for example  com.sun.net.httpserver.HttpServer.

Yet for CommandLine parsing I used apache-cli and apache commons for one ENUM- Utility functions. 



- The Server uses a simple Executor Service to handle several Requests/ Threads at the same time.

- The Keep-Alive feature is implemented

- The Server Implements GET,POST, PUT, DELETE, HEAD, TRACE and OPTION

- GET returns the requested resource

- PUT creates the resource, e.g. curl -X PUT --data-binary @d:/picture.jpeg http://localhost:8082/picture.jpeg. / can not be overwritten.

- POST here behaves exactly like curl, although depending on specific use cases one wants to implement more specific behavior. As this was not specified in the task given, it just writes the posted file to the given path

- DELETE deletes the provided resource. Deleting / is not possible. (Though one could specify inde.xhtml here and it would be deleted)

- HEAD requests - as get without body

- OPTIONS behaves like the options request in tomcat

- TRACE behaves like the trace request in tomcat (basically like options, but returning 405)


- While it implements the Keep-Alive behavior the implementation is not very sophisticated as
  I could not find a way to listen to incoming traffic on an  already used connection without using java-nio. If Keep-Alive= true the WebServerThread recursively calls it's handleRequest Method to wait for incoming traffic on the same Client Socket. The timeout is configureable.
  
- 	For each HTTP-Method a dedicated handler is implemented. Behavior is not reused- so a refactoring could clearly improve this (out of time, sorry).

- For logging I used slf4j with a simple logging binder to log to the console only. For a more sophisticated product this  could easily be improved by using the log4j binding for example. Yet for this demo I considered it adequate.


- Unit-Test coverage is not adequate. I used mainly Chrome, Curl and Postman for Testing, not Unit Tests. This is clearly a point to be improved. (Ultimately i removed all unit tests and the JUnit dependencies as the tests were useless at this point.)

The Server clearly does not implement all HTTP behavior, e.g. no chuncked transfers, no Cookies, no Cache Control or E-Tags or Even SSL/TLS.


On every request a ">" is printed to STDOUT,  which is automatically prefixed by the pool (uninteresting as there is only one) and the thread.  
In case the keep-alive is working te same thread is used again. Yet this is also browser- dependent as for Firefox this worked reliably, for chrome sometimes new connections were opened.



