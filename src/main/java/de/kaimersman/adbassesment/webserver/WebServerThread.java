package de.kaimersman.adbassesment.webserver;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.StringTokenizer;

import org.apache.commons.lang3.EnumUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.kaimersman.adbassesment.webserver.handler.response.HttpHandler;
import de.kaimersman.adbassesment.webserver.handler.response.HttpHandlerFactory;

import de.kaimersman.adbassesment.webserver.http.HttpMethodEnum;
import de.kaimersman.adbassesment.webserver.http.HttpRequest;

public class WebServerThread implements Runnable{

	final static Logger logger = LoggerFactory.getLogger(WebServerThread.class);
	private final Socket socket;
	private final String contentDir;
	private final int keeepAliveTimeout;
	private HttpRequest httpRequest = null;
	
	public WebServerThread(Socket socket, String contentDir, int keepAliveTimeout) {

		this.socket = socket;
		this.contentDir = contentDir;
		this.keeepAliveTimeout = keepAliveTimeout;
	}

	@Override
	/**
	 * Thread is started to handle Http-Request.
	 */
	public void run() {

		handleHttpRequest();
	}
	
	
	/**
	 * Method to handle a single Http-Request from  a client. 
	 * In case keep-alive is used, this method is called recursively to read the nexrt request on the same client socket.
	 */
	public void handleHttpRequest(){
			logger.info(">");
	
		try {
			
			// Open the Input Stream to read from 
			DataInputStream in = new DataInputStream(socket.getInputStream());
			
			try {
			// Read the whole Request
			httpRequest = readRequest(in);
			}
			catch(SocketTimeoutException socketTimeoutException) {
				
				logger.info("Socket Timeout occured- This expected behavior in case no further requests are receeived for an open connection after keep-alive timeout. ");
				return;
				
			}
			
			
			if(httpRequest.getRequestType() == null) {
				return;
			}
			
			// create Response and return to client
			processRequest(httpRequest);
		} 
		
		catch (IOException e) {
			logger.info(e.getMessage());
			e.printStackTrace();
			
	    }
	}
	
	/**
	 * Reads the complete request with all relevant header-fields and the request body as byte array
	 * @param in
	 * @return HttpRequest innstance, representing the a complete (single) HttpRequest made by the client.
	 * @throws SocketTimeoutException
	 * @throws IOException
	 */
	public HttpRequest readRequest(DataInputStream in) throws SocketTimeoutException, IOException{
		
		// Source:
		// https://www.experts-exchange.com/questions/20513008/How-to-read-HTTP-Post-Request-in-Socket.html

		HttpRequest httpRequest = new HttpRequest();
		String line;
		int content_length = 0, readed, c;
		//DataInputStream in;
		byte[] buffer = new byte[1024];
		
			in = new DataInputStream(socket.getInputStream());
			while ((line = in.readLine()) != null) {
			
				// logger.info(line);   // Uncomment to print header lines
				
				if("".equals(line))
						break; // The HTTP header is complete ;
				
				StringTokenizer headerlineTokenizer = new StringTokenizer(line, " ");
				String key = headerlineTokenizer.nextToken();

				
				// The action line in the Http -Request is special as it holdes 3 attributes (method, requested Resource, Protocol
				if (EnumUtils.isValidEnum(HttpMethodEnum.class, key.toUpperCase())) {

					httpRequest.setRequestType(HttpMethodEnum.valueOf(key.toUpperCase()));
					httpRequest.setRequestedResource(headerlineTokenizer.nextToken());
					httpRequest.setProtocol(headerlineTokenizer.nextToken());

					continue;
				}

				switch (key.toUpperCase()) {
				case "CONTENT-LENGTH:":
					httpRequest.setContentLength(new Integer(headerlineTokenizer.nextToken()));
					break;

				case "CONTENT-TYPE:":
					httpRequest.setContentType(headerlineTokenizer.nextToken());
					break;

				case "ACCEPT:":
					httpRequest.setAccept(headerlineTokenizer.nextToken());
					break;

				case "EXPECT:":
					httpRequest.setExpect(headerlineTokenizer.nextToken());
					break;

				case "HOST:":
					httpRequest.setHost(headerlineTokenizer.nextToken());
					break;

				case "CACHE-CONTROL:":
					String cacheControl = headerlineTokenizer.nextToken();
					httpRequest.setCacheControl(cacheControl);
					break;

				case "USER-AGENT:":
					httpRequest.setUserAgent(headerlineTokenizer.nextToken());
					break;

				case "CONNECTION:":
					String keepalive = headerlineTokenizer.nextToken();
					if ("close".equals(keepalive)) {
						httpRequest.setKeepAlive(false);
					} else {
						httpRequest.setKeepAlive(true);
					}
					break;
				}

			}
	
			
			// Read the HTTP request body
			// The body is added as Byte Array to the httpRequest Objec
			if (httpRequest.getContentLength() > 0) {
				readed = 0;
				ByteArrayOutputStream bos = new ByteArrayOutputStream(httpRequest.getContentLength());

				while ((c = in.read(buffer, 0, 1024)) != -1) {
					bos.write(buffer, 0, c);
					readed += c;
					if (readed >= httpRequest.getContentLength())
						break;
				}

				bos.flush();
				bos.close();
				bos.toByteArray();
				httpRequest.setBody(bos.toByteArray());
			}
		
		
		return httpRequest;
			
	}

	
	
	/**
	 * The processRequest method fetches the correct handler based on the Http-Method.
	 * The Http-Method specific handler writes the response back to the client.
	 * The process Request method flushes the Output Stream and sets the Keep-Alive on the socket based 
	 * on the keep-alive settings in the client's request (or as a default if not defined in the client)
	 * 
	 * @param httpRequest
	 * @throws IOException
	 */
	public void processRequest(HttpRequest httpRequest)  throws IOException{

		try {
		
			HttpHandler httpMethodHandler = HttpHandlerFactory.getHandlerForHttpMethod(httpRequest.getRequestType(),contentDir);
			httpMethodHandler.handleHttpMethod(httpRequest, new PrintWriter(socket.getOutputStream()),new BufferedOutputStream(socket.getOutputStream()), keeepAliveTimeout);
			
			socket.getOutputStream().flush();

			
			
			if (httpRequest.isKeepAlive()) {
				// Prepare Socket for Keep-Alive and recursively handleRequest Method to listen for next request on the open connection
				socket.setKeepAlive(true);
				socket.setSoTimeout(keeepAliveTimeout*1000);
				handleHttpRequest();
			} else {
				// Close socket explicitly,  keep-alive= false
				socket.getOutputStream().flush();
				socket.getOutputStream().close();
				socket.close();
			}
				
			
		}
		catch(SocketTimeoutException e) {

			// After a Socket Timeout (on a connection which was kept open (keep-alive = true) socket is closed;
			socket.setKeepAlive(false);
			forceSocketClose(socket);
		}

	}

	
	/**
	 * Try to close Socket - on failure log messages are printed, 
	 * yet no Exception is thrown as this can not be handled in a meaningful way.
	 * @param s
	 */
	private void forceSocketClose(Socket s) { 
	
		if(! socket.isClosed()) {
			try {
				logger.warn("Trying to close socker after Error");
				socket.close();
			} catch (IOException e) {
				logger.warn("Closing socket after Error failed");
				e.printStackTrace();
			}
	 }

	}
}
