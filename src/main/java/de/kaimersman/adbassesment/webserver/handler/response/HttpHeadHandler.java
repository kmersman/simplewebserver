package de.kaimersman.adbassesment.webserver.handler.response;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.kaimersman.adbassesment.webserver.http.HttpRequest;

public class HttpHeadHandler extends AbstractHttpHandler {
	final static Logger logger = LoggerFactory.getLogger(HttpHeadHandler.class);

	final String contentDir;

	public HttpHeadHandler(String contentDir) {

		this.contentDir = contentDir;

	}

	@Override
	public void handleHttpMethod(HttpRequest httpRequest, PrintWriter out, BufferedOutputStream dataOut,
			int keepAliveTimeout) throws IOException {

		sendFile(out, dataOut, httpRequest, keepAliveTimeout);
		out.flush();
		dataOut.flush();

	}

	private void sendFile(PrintWriter out, BufferedOutputStream dataOut, HttpRequest httpRequest, int keepAliveTimeout)
			throws IOException {

		String fileToDeliver = httpRequest.getRequestedResource();
		if ("/".equals(fileToDeliver)) {

			fileToDeliver = "/index.html";
		}

		// TODO read baseDir from reasonable location
		String absoluteFileName = contentDir + fileToDeliver;

		if (checkFile(absoluteFileName)) {

			Path path = Paths.get(absoluteFileName);
			File file = path.toFile();
			long contentLength = file.length();
			String contentType = Files.probeContentType(path);

			out.println("HTTP/1.1 200 OK");
			out.println(SERVER_STRING);
			out.println("Date: " + new Date());

			out.println("Content-type: " + contentType);
			out.println("Content-length: " + new Long(contentLength).toString());

			if (httpRequest.isKeepAlive()) {
				out.println("Keep-Alive: timeout=+" + keepAliveTimeout + " , max:100");
			}

			out.println();

		} else {
			sendNotFound(out);
		}

	}

	private boolean checkFile(String absoluteFileName) {

		File f = Paths.get(absoluteFileName).toFile();
		logger.info(absoluteFileName + " exists ? " + f.exists());
		return f.exists() && f.isFile() && f.canRead();

	}

}
