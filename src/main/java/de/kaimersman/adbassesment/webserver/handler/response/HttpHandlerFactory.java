package de.kaimersman.adbassesment.webserver.handler.response;

import de.kaimersman.adbassesment.webserver.http.HttpMethodEnum;

public class HttpHandlerFactory {

	
	public static HttpHandler getHandlerForHttpMethod(HttpMethodEnum method, String contentDir) {
		
		
		
		if(method == null) {
			
		"".toString();	
			
		}
		
		switch(method) {
		case GET:
			return new HttpGetHandler(contentDir); 
		case POST:
			return new HttpPostHandler(contentDir);
			
		case PUT:
			return new HttpPutHandler(contentDir);

		case DELETE:
			return new HttpDeleteHandler(contentDir);

			
		case HEAD:	
			return new HttpHeadHandler(contentDir);
	
		case OPTIONS:	
			return new HttpOptionsHandler(contentDir);
	
		case TRACE:	
			return new HttpTraceHandler(contentDir);
	
			
			
		default:
			return new HttpMethodNotImplementedHandler();
			
		}

	}
	
}
