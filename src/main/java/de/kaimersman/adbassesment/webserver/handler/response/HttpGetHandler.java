package de.kaimersman.adbassesment.webserver.handler.response;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.kaimersman.adbassesment.webserver.http.HttpRequest;

public  class HttpGetHandler extends AbstractHttpHandler {
	
	final static Logger logger = LoggerFactory.getLogger(HttpGetHandler.class);
	final String contentDir;
	public HttpGetHandler(String contentDir) {
		
		this.contentDir = contentDir;
		
	}

	@Override
	public void handleHttpMethod(HttpRequest httpRequest, PrintWriter out, BufferedOutputStream dataOut, int keepAliveTimeout) throws IOException {
		
		sendFile(httpRequest,out,dataOut, keepAliveTimeout);
		dataOut.flush();
	}
	
	private void sendFile(HttpRequest httpRequest, PrintWriter out, BufferedOutputStream dataOut, int keepAliveTimeout) throws IOException {

		String fileToDeliver = httpRequest.getRequestedResource();
		if("/".equals(fileToDeliver)) {
			
			fileToDeliver="/index.html";
		}
		
		String absoluteFileName = contentDir + fileToDeliver;

		if (checkFile(absoluteFileName)) {

			Path path = Paths.get(absoluteFileName);
			File file = path.toFile();
			long contentLength = file.length();
			String contentType = Files.probeContentType(path);
		
			out.println("HTTP/1.1 200 OK");
			out.println(SERVER_STRING);
			out.println("Date: " + new Date());
			
			out.println("Content-type: " + contentType);
			out.println("Content-length: " + new Long(contentLength).toString());
			printKeepAliveHeader(httpRequest, out, keepAliveTimeout);
			
			out.println();
			
			// As the body is written in the data stream afterwards the PrintWrite encapsulating the OutputStream needs to be flushed here
			out.flush();
			// "Copy" file from  contentDirectory to OutputStream of the Socket
			Files.copy(path, dataOut);

		} else {
			sendNotFound(out);	
			}

	}

	private boolean checkFile(String absoluteFileName) {

		File f = Paths.get(absoluteFileName).toFile();
		return f.exists() && f.isFile() && f.canRead();

	}


}
