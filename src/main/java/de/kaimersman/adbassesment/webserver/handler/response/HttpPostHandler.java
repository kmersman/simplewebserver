package de.kaimersman.adbassesment.webserver.handler.response;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.kaimersman.adbassesment.webserver.http.HttpRequest;

public class HttpPostHandler extends AbstractHttpHandler {

	final static Logger logger = LoggerFactory.getLogger(HttpPostHandler.class);

	final String contentDir;

	public HttpPostHandler(String contentDir) {

		this.contentDir = contentDir;

	}

	private void writeContent(String requestedResource, byte[] content) throws IOException {

		String target = contentDir + requestedResource;

		FileOutputStream fos = new FileOutputStream(target);
		fos.write(content);
		fos.flush();
		fos.close();

	}

	public void handleHttpMethod(HttpRequest httpRequest, PrintWriter out, BufferedOutputStream dataOut,
			int keepAliveTimeout) throws IOException {

		writeContent(httpRequest.getRequestedResource(), httpRequest.getBody());
		out.println("HTTP/1.1 200 OK");
		out.println(SERVER_STRING);
		out.println("Date: " + new Date());
		out.println("Content-Length:  0");
		if (httpRequest.isKeepAlive()) {
			out.println("Keep-Alive: timeout=+" + keepAliveTimeout + " , max:100");
		}
		out.println();
	}
}
