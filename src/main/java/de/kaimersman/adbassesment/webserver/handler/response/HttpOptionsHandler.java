package de.kaimersman.adbassesment.webserver.handler.response;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.kaimersman.adbassesment.webserver.http.HttpRequest;

public class HttpOptionsHandler extends AbstractHttpHandler {
	final static Logger logger = LoggerFactory.getLogger(HttpOptionsHandler.class);

	final String contentDir;

	public HttpOptionsHandler(String contentDir) {

		this.contentDir = contentDir;

	}

	@Override
	public void handleHttpMethod(HttpRequest httpRequest, PrintWriter out, BufferedOutputStream dataOut,
			int keepAliveTimeout) throws IOException {

		out.println("HTTP/1.1 200");
		out.println("Allow: HEAD, DELETE, POST, GET, OPTIONS, PUT");
		out.println("Content-Length: 0");

		if (httpRequest.isKeepAlive()) {
			out.println("Keep-Alive: timeout=+" + keepAliveTimeout + " , max:100");
		}

		out.println("Date: " + new Date());
		out.println();
		
	}

}
