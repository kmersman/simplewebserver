package de.kaimersman.adbassesment.webserver.handler.response;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import de.kaimersman.adbassesment.webserver.http.HttpRequest;

public interface HttpHandler {
	public  void handleHttpMethod(HttpRequest httpRequest, PrintWriter out, BufferedOutputStream dataOut, int keepAliveTimeout) throws IOException;
}
