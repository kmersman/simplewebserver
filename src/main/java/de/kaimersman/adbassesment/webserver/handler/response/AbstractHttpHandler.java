package de.kaimersman.adbassesment.webserver.handler.response;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

import de.kaimersman.adbassesment.webserver.http.HttpRequest;

public abstract class AbstractHttpHandler implements HttpHandler {

    static final String SERVER_STRING = "Simple WebServer 1.0";
	
	public abstract void handleHttpMethod(HttpRequest httpRequest, PrintWriter out, BufferedOutputStream dataOut, int keepAliveTimeout) throws IOException;

	public  void sendNotFound(PrintWriter out) {
		out.println("HTTP/1.0 404 Not Found");
		out.println();
	}

	void printKeepAliveHeader(HttpRequest httpRequest, PrintWriter out, int keepAliveTimeout) {
		
		
		if(httpRequest.isKeepAlive()) {
			out.println("Keep-Alive: timeout=" +keepAliveTimeout+" , max:100");
		}
		
	}

	
	}
