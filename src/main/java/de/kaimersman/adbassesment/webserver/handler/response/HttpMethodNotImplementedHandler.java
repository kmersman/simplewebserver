package de.kaimersman.adbassesment.webserver.handler.response;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import de.kaimersman.adbassesment.webserver.http.HttpRequest;

public class HttpMethodNotImplementedHandler extends AbstractHttpHandler {

	public void handleHttpMethod(HttpRequest httpRequest, PrintWriter out, BufferedOutputStream dataOut, int keepAliveTimeout)
			throws IOException {
		out.println("HTTP/1.1 501 Not Implemented");
		
		if(httpRequest.isKeepAlive()) {
			out.println("Keep-Alive: timeout=+" +keepAliveTimeout+" , max:100");
		}
		out.println();
		

	}

}
