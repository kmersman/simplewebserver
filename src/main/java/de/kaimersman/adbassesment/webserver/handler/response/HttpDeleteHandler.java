package de.kaimersman.adbassesment.webserver.handler.response;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import de.kaimersman.adbassesment.webserver.http.HttpRequest;

public class HttpDeleteHandler extends AbstractHttpHandler {

	final String contentDir;

	public HttpDeleteHandler(String contentDir) {

		this.contentDir = contentDir;

	}

	private void deleteContent(String requestedResource, PrintWriter out) throws IOException {

		String target = contentDir + requestedResource;

		File f = new File(target);

		if (!f.exists()) {
			out.println("HTTP/1.1 404 Not Found");
			return;
		}

		if (!f.canWrite()) {
			out.println("HTTP/1.1 403 Forbidden");
			return;
		}

		boolean success = f.delete();
		if (success) {
			out.println("HTTP/1.1 200 OK");
			return;
		} else {
			out.println("HTTP/1.1 500 Internal Server Error");
			return;
		}

	}

	public void handleHttpMethod(HttpRequest httpRequest, PrintWriter out, BufferedOutputStream dataOut,
			int keepAliveTimeout) throws IOException {

		if ("/".equals(httpRequest.getRequestedResource())) {
			out.println("HTTP/1.1 403 Forbidden");

		} else {

			deleteContent(httpRequest.getRequestedResource(), out);
		}

		out.println(SERVER_STRING);
		out.println("Date: " + new Date());
		printKeepAliveHeader(httpRequest, out, keepAliveTimeout);


		out.println("Content-Length:  0");
		out.println();
	}

}
