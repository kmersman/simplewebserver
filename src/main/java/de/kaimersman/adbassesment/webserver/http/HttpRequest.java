package de.kaimersman.adbassesment.webserver.http;

import java.util.HashMap;

import de.kaimersman.adbassesment.webserver.HttpRequestConstants;

public class HttpRequest implements HttpRequestConstants{

	
	// HashMap<String, String> headerAttributes = new HashMap<String, String> ();
	
	
	private byte[] body;
	private HttpMethodEnum requestType;
	private HTTPProtocolVersion version;
	private String UserAgent;
	private String Host;
	private String requestedResource;
	private boolean keepAlive;
	private String protocol;
	private String cacheControl;
	private int contentLength;
	private String contentType;
	private String Expect;
	private String Accept;
	
	
	
	
	public HTTPProtocolVersion getVersion() {
		return version;
	}
	public void setVersion(HTTPProtocolVersion version) {
		this.version = version;
	}
	public String getUserAgent() {
		return UserAgent;
	}
	public void setUserAgent(String userAgent) {
		UserAgent = userAgent;
	}
	public String getHost() {
		return Host;
	}
	public void setHost(String host) {
		Host = host;
	}
	public String getRequestedResource() {
		return requestedResource;
	}
	public void setRequestedResource(String requestedResource) {
		this.requestedResource = requestedResource;
	}
	
	public boolean isKeepAlive() {
		return keepAlive;
	}
	public void setKeepAlive(boolean keepAlive) {
		this.keepAlive = keepAlive;
	}
	
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public HttpMethodEnum getRequestType() {
		return requestType;
	}
	public void setRequestType(HttpMethodEnum requestType) {
		this.requestType = requestType;
	}
	
	public String getCacheControl() {
		return cacheControl;
	}
	public void setCacheControl(String cacheControl) {
		this.cacheControl = cacheControl;
	}
	public int getContentLength() {
		return contentLength;
	}
	public void setContentLength(int contentLength) {
		this.contentLength = contentLength;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getExpect() {
		return Expect;
	}
	public void setExpect(String expect) {
		Expect = expect;
	}
	public String getAccept() {
		return Accept;
	}
	public void setAccept(String accept) {
		Accept = accept;
	}
	@Override
	public String toString() {
		return "HttpRequestHeader [requestType=" + requestType + ", version=" + version + ", UserAgent=" + UserAgent
				+ ", Host=" + Host + ", requestedResource=" + requestedResource + ", keepAlive=" + keepAlive
				+ ", protocol=" + protocol + ", cacheControl=" + cacheControl + ", contentLength=" + contentLength
				+ ", contentType=" + contentType + ", Expect=" + Expect + ", Accept=" + Accept + "]";
	}
	public byte[] getBody() {
		return body;
	}
	public void setBody(byte[] body) {
		this.body = body;
	}
}
