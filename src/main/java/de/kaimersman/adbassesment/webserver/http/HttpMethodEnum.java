package de.kaimersman.adbassesment.webserver.http;

public enum HttpMethodEnum {

	GET,HEAD,POST,PUT,DELETE,CONNECT,OPTIONS,TRACE,PATCH
}
