package de.kaimersman.adbassesment.webserver;

import static de.kaimersman.adbassesment.webserver.DefaultConfiguration.*;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class WebServerRunner {

	final static Logger logger = LoggerFactory.getLogger(WebServerRunner.class);

	int port;
	int keepAliveTimeout;
	int threadPoolSize;
	String contentDir;
	String logLevel;
	
	ExecutorService executorService = null;

	public WebServerRunner(String args[]) throws IOException {

		initializeAndRun(args);

	}

	private void initializeAndRun(String args[]) throws IOException {

		try {
			// Setup Command Line �arser
			CommandLineParser cliParser = new DefaultParser();

			// Parse Commad Line
			CommandLine commandLine = cliParser.parse(getCliOptions(), args);

			// Create Config Object
			processCommandLineParameters(commandLine);

		} catch (ParseException ex) {

			port = DEFAULT_PORT;
			threadPoolSize = DEFAULT_THREADPOOL_SIZE;
			logLevel = DEFAULT_LOGLEVEL;

			logger.warn("Exception parsing Command Line Arguments, using Default values");

		} finally {

			
			if(!(contentDir.endsWith("/"))) {
				
				contentDir = contentDir+"/";
				
			}
			
			Path absolutePath = Paths.get(contentDir).toAbsolutePath();
			
			logger.info("Starting WebServer listening to port " 
			+ port 
			+ " with ThreadPoolSize "
			+ threadPoolSize
			+ " and serving content from " +absolutePath.toString());
					
					
			serverLoop();
		
		}

	}

	/**
	 * Instantiate new WebServerRunner and pass CmdLine Arguments
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		if (args.length < 1) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("WebServerRunner", getCliOptions());
			System.exit(0);
		}
		
		
		WebServerRunner wsrunner = new WebServerRunner(args);
	}

	/**
	 * Main Server Loop - accepts Socket connections and stars new Thread of class
	 * WebServerThread
	 * 
	 * @param port
	 * @param poolSize
	 * @param workdir
	 * @throws IOException
	 */
	public void serverLoop() throws IOException {

		executorService = Executors.newFixedThreadPool(threadPoolSize);
		ServerSocket serverSocket = new ServerSocket(port);

		while (true) {

			Socket cliSocketConnetion = serverSocket.accept();

			WebServerThread th = new WebServerThread(cliSocketConnetion, contentDir,keepAliveTimeout);
			logger.info("Executing new Thread: " + th);
			executorService.execute(th);
		}

	}

	/**
	 * Determine the Directory content will be served from
	 * 
	 * @param path
	 * @return
	 */
	private static Path getWorkdir(String path) {
		return Paths.get(path).toAbsolutePath();
	}

	/**
	 * Define the Command Line Options to configure SimpleWebSerer
	 * 
	 * @return
	 */
	private static Options getCliOptions() {


		Options options = new Options();
		options.addOption("d", "directory", true,
				"MANDATORY Directory to serve static HTML Content from");
		options.addOption("p", "port", true, "WebServer Port to run on, defaults to" + DEFAULT_PORT);
		options.addOption("t", "threads", true,
				"Size of the ThreadPool, defaults to " + DEFAULT_THREADPOOL_SIZE);
		
		options.addOption("k", "keep-alive timeout", true,
				"Keep-Alive Timeout, defaults to " + DEFAULT_KEEPALIVE_TIMEOUT + " seconds");
		
		
		return options;
	}

	/**
	 * Create Config Object based on Command Line Parameters. User Defaults if
	 * Parameter is not specified on the commandLine
	 * 
	 * @param commandLine
	 * @return Config to be used by SimpleWebServer
	 */
	private void processCommandLineParameters(CommandLine commandLine) {



	

		if (commandLine.hasOption("p")) {
			String optionval = commandLine.getOptionValue("p");
			port = Integer.parseInt(optionval);
		} else {

			port = DEFAULT_PORT;
		}


		if (commandLine.hasOption("k")) {
			String optionval = commandLine.getOptionValue("k");
			keepAliveTimeout = Integer.parseInt(optionval);
		} else {

			keepAliveTimeout = DEFAULT_KEEPALIVE_TIMEOUT;
		}


		if (commandLine.hasOption("t")) {
			String optionval = commandLine.getOptionValue("t");
			threadPoolSize = Integer.parseInt(optionval);
		} else {

			threadPoolSize = DEFAULT_THREADPOOL_SIZE;		}

		if (commandLine.hasOption("d")) {
			String optionval = commandLine.getOptionValue("d")+"/";
			contentDir = optionval;
		} else {
			
			logger.error("Please provide parameter -d <html-content-directory>, e.g. -d ../html");
			System.exit(1);
		}


	}

}
