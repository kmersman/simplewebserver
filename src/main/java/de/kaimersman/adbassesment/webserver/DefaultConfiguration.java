package de.kaimersman.adbassesment.webserver;

public interface DefaultConfiguration {
	
	public static final String DEFAULT_LOGLEVEL="INFO";
	//public static final String DEFAULT_CONTENT_DIR="./html";
	public int DEFAULT_PORT=8080;
	public int DEFAULT_THREADPOOL_SIZE=3;
	public int DEFAULT_KEEPALIVE_TIMEOUT=10;
}
