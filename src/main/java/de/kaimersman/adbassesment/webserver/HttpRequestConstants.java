package de.kaimersman.adbassesment.webserver;

public interface HttpRequestConstants {

	
	public String HTTP_HEADER_CONTENT_TPE ="Content-Type";
	public String HTTP_HEADER_CACHE_CONTROL="cache-control";
	public String HTTP_HEADER_USER_AGENT="User-Agent"; 
	public String HTTP_HEADER_ACCEPT="Accept";
	public String HTTP_HEADER_HOST="Host";
	public String HTTP_HEADER_ACCEPT_ENCODING="accept-encoding";
	public String HTTP_HEADER_CONTENT_LENGTH="content-length";
	public String HTTP_HEADER_CONNECTION="Connection";

	
}
